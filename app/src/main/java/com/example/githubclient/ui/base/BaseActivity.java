package com.example.githubclient.ui.base;

import android.app.ProgressDialog;
import android.os.Bundle;

import com.example.githubclient.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initProgressDialog();
    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
    }

    protected abstract int getFragmentContainerId();

    protected void showLoading() {
        if (!progressDialog.isShowing()) {
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.show();
        }
    }

    protected void showLoading(String message) {
        if (!progressDialog.isShowing()) {
            progressDialog.setMessage(message);
            progressDialog.show();
        }
    }

    protected void hideLoading() {
        if (progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }
}
